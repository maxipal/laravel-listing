<?php

namespace Stylemix\Listing\Attribute;

use Illuminate\Support\Arr;
use Spatie\QueryBuilder\AllowedFilter;
use Stylemix\Listing\Contracts\Filterable;
use Stylemix\Listing\Contracts\Sortable;
use Stylemix\Base\QueryBuilder\DatetimeFilter;

class Datetime extends Base implements Sortable, Filterable
{
	use AppliesNumericQuery, AppliesDefaultSort;

	/**
	 * Adds attribute mappings for elastic search
	 *
	 * @param \Illuminate\Support\Collection $mapping Mapping to modify
	 *
	 * @return void
	 */
	public function elasticMapping($mapping)
	{
		$mapping[$this->name] = ['type' => 'date'];
	}

	/**
	 * Adds attribute casts
	 *
	 * @param \Illuminate\Support\Collection $casts
	 */
	public function applyCasts($casts)
	{
		$casts->put($this->name, 'datetime');
	}

	/**
	 * @inheritDoc
	 */
	public function applyFilter($criteria, $filter)
	{
		$filterField = $this->filterField ?? $this->name;

		if (is_array($criteria) && Arr::isAssoc($criteria)) {
			$range = array_filter(
				(array) $criteria + ['gt' => null, 'gte' => null, 'lt' => null, 'lte' => null],
				function ($value) {
					return $value !== null;
				}
			);

			if (count($range)) {
				$range = array_map($this->integer ? 'intval' : 'strval', $range);
				$filter[$this->name] = ['range' => [$filterField => $range]];
			}

			return;
		}

		$criteria = array_map($this->integer ? 'intval' : 'floatval', Arr::wrap($criteria));
		$filter[$this->name] = ['terms' => [$filterField => $criteria]];	}

}
