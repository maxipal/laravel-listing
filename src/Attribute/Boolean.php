<?php

namespace Stylemix\Listing\Attribute;

use Stylemix\Listing\Contracts\Filterable;
use Stylemix\Listing\Contracts\Searchable;
use Stylemix\Listing\Contracts\Sortable;

class Boolean extends Base implements Filterable, Sortable, Searchable
{

	/**
	 * Adds attribute mappings for elastic search
	 *
	 * @param \Illuminate\Support\Collection $mapping Mapping to modify
	 */
	public function elasticMapping($mapping)
	{
		$mapping[$this->name] = ['type' => 'boolean'];
	}

	public function applyCasts($casts)
	{
		$casts[$this->name] = 'boolean';
	}

	public function applyFilter($criteria, $filter)
	{
		$filter[$this->name] = ['term' => [$this->name => (boolean) $criteria]];
	}

	/**
	 * @inheritdoc
	 */
    public function applySort($criteria, $sort, $key): void
	{
		$sort->put($key, [
			$this->name => $criteria,
		]);
	}
}
