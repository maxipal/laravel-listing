<?php

namespace Stylemix\Listing\Attribute;

use Illuminate\Support\Arr;
use Stylemix\Listing\Contracts\Filterable;
use Stylemix\Listing\Contracts\Sortable;

class Location extends Base implements Filterable, Sortable
{

	/**
	 * @inheritdoc
	 */
	public function __construct(string $name = null)
	{
		$name = $name ?? 'location';
		parent::__construct($name);
	}

	/**
	 * @inheritdoc
	 */
	public function elasticMapping($mapping)
	{
		$mapping[$this->name] = [
			'properties' => [
				'latlng' => ['type' => 'geo_point'],
				'zoom' => ['type' => 'integer'],
				'address' => ['type' => 'keyword'],
				'city' => ['type' => 'keyword'],
				'zip' => ['type' => 'keyword'],
				'region' => ['type' => 'keyword'],
				'country' => ['type' => 'keyword'],
				'two_letter' => ['type' => 'keyword'],
				'distance' => ['type' => 'integer']
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function applyFilter($criteria, $filter)
	{
	    if(empty($criteria['latlng'])) return;
		$distance = '';
		if(!empty($criteria['distance'])) {
			if(strpos($criteria['distance'], 'km')) $distance = $criteria['distance'];
			else if(strpos($criteria['distance'], 'mi')) $distance = $criteria['distance'];
			else $distance = $criteria['distance']."km";
		}
		$filter[$this->name] = [
			'geo_distance' => [
				$this->name . '.latlng' => $criteria['latlng'],
				'distance' => !empty($distance) ? $distance : 10000,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function applySort($criteria, $sort, $key) : void
	{
		if (is_string($criteria) && strpos($criteria, '|') !== false) {
			$criteria = explode('|', $criteria);
		}

		$criteria = Arr::wrap($criteria);
		$criteria += [
			1 => 'asc',
			2 => $this->attributes['unit'] ?? 'm'
		];

		$sort->put($key, [
			'_geo_distance' => [
				$this->name . '.latlng' => $criteria[0],
				'order' => $criteria[1],
				'unit' => $criteria[2],
				'mode' => 'min',
				'distance_type' => 'arc'
			],
		]);
	}
}
